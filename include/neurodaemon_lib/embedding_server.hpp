#pragma once

#include "neurodaemon_lib/connection_handler.hpp"
#include "neurodaemon_lib/thread_manager.hpp"
#include "neurodaemon_lib/thread_pool.hpp"
#include "neurodaemon_lib/threaded_handler.hpp"

#include <atomic>
#include <chrono>
#include <cstring>
#include <iostream>
#include <mutex>
#include <stdexcept>
#include <string>
#include <string_view>
#include <sys/socket.h>
#include <utility>

using namespace std::chrono_literals;

namespace neurodaemon
{
const int MAX_TASKS = 1024;
const int LOW_WATERMARK = 8;
const int HIGH_WATERMARK = 8;
const int TASKS_WATERMARK_THRESHOLD = 2;
const auto ABOVE_WATERMARK_TIMEOUT = 5s;

enum class ThreadingMode
{
    single_thread,
    thread_per_request,
    thread_pool,
};

template <typename TextEmbedder, typename ThreadManager> class EmbeddingServer
{
  public:
    using Handler = ConnectionHandler<TextEmbedder>;

    EmbeddingServer(TextEmbedder &emb, int sfd, ThreadingMode threading_mode,
                    PinMode pin_mode, bool keepalive)
        : handler_(emb, keepalive, running_), sfd_(sfd),
          threading_mode_(threading_mode), thread_mgr_(pin_mode)
    {
        if (threading_mode == ThreadingMode::thread_per_request)
        {
            threaded_handler_.emplace(handler_, thread_mgr_, cerr_mutex_);
        }
        else if (threading_mode == ThreadingMode::thread_pool)
        {
            thread_pool_.emplace(LOW_WATERMARK, HIGH_WATERMARK,
                                 ABOVE_WATERMARK_TIMEOUT, MAX_TASKS,
                                 TASKS_WATERMARK_THRESHOLD, running_, handler_,
                                 thread_mgr_);
            thread_pool_->InitThreads();
        }
    }

    void RunMainThread()
    {
        while (running_)
        {
            auto err = DoWork();
            if (!err.empty())
            {
                std::lock_guard<std::mutex> cerr_lock{cerr_mutex_};
                std::cerr << "RunMainThread exited with " << err << std::endl;
            }
        }
    }

    void Shutdown()
    {
        running_ = false;
        for (int cfd : cfds_)
        {
            shutdown(cfd, SHUT_RD);
        }
        if (thread_pool_)
        {
            thread_pool_->NotifyAll();
        }
    }

    ~EmbeddingServer()
    {
        std::cout << "Joining threads" << std::endl;
        thread_mgr_.Join();
        std::cout << "Joined threads" << std::endl;
        if (threading_mode_ == ThreadingMode::thread_pool)
        {
            thread_pool_->CheckFutures(true);
        }
    }

  private:
    std::pair<std::optional<ConnectionSocket>, std::string> AcceptClient()
    {
        int cfd = accept(sfd_, nullptr, nullptr);
        if (cfd == -1)
        {
            if (errno == EINTR && !running_)
            {
                return {std::nullopt, ""};
            }
            return {std::nullopt, strerror(errno)};
        }
        cfds_.push_back(cfd);
        return {std::make_optional<ConnectionSocket>(cfd), ""};
    }

    std::string HandleClient(ConnectionSocket connection)
    {
        std::string err;
        if (threading_mode_ == ThreadingMode::single_thread)
        {
            err = handler_.HandleClient(std::move(connection));
        }
        else if (threading_mode_ == ThreadingMode::thread_per_request)
        {
            threaded_handler_->HandleClient(std::move(connection));
        }
        else if (threading_mode_ == ThreadingMode::thread_pool)
        {
            err = thread_pool_->HandleClient(std::move(connection));
        }
        return err;
    }

    std::string DoWork()
    {
        auto [connection, err] = AcceptClient();
        if (!err.empty())
        {
            return err;
        }
        if (!connection)
        {
            return "";
        }
        return HandleClient(std::move(*connection));
    }

    std::atomic<bool> running_ = true;
    std::mutex cerr_mutex_;
    std::optional<ThreadPool<Handler, ThreadManager>> thread_pool_;
    std::optional<ThreadedHandler<Handler, ThreadManager>> threaded_handler_;
    std::vector<int> cfds_;
    Handler handler_;
    int sfd_;
    ThreadingMode threading_mode_;
    ThreadManager thread_mgr_;
};
} // namespace neurodaemon
