#pragma once

#include "neurodaemon_lib/connection_socket.hpp"

#include "base64/base64.hpp"
#include "neuro.pb.h"
#include "neurodaemon_lib/connection_socket.hpp"
#include "text_process/neuro_predict.hpp"

#include <atomic>
#include <string>
#include <string_view>
#include <utility>

namespace neurodaemon
{
template <typename TextEmbedder> class ConnectionHandler
{
  public:
    ConnectionHandler(TextEmbedder &emb, bool keepalive,
                      const std::atomic_bool &running)
        : emb_(emb), keepalive_(keepalive), running_(running)
    {
    }

    std::pair<std::string, std::string>
    ComputeAnswer(std::string_view encoded_request)
    {
        text_process::NeuroAnswer answer;
        auto [protobuf_bytes, err] = base64::decode(encoded_request);
        if (!err.empty())
        {
            return {"", "Protobuf decoding error: "};
        }
        text_process::NeuroRequest request;
        if (!request.ParseFromString(protobuf_bytes))
        {
            return {"", "Protobuf parsing error"};
        }
        answer.set_qid(request.qid());
        auto embedding = emb_.Encode(request.query());
        *answer.mutable_embedding() = {embedding.begin(), embedding.end()};
        std::string serialized_answer;
        if (!answer.SerializeToString(&serialized_answer))
        {
            return {"", "Protobuf serialization error"};
        }
        std::string encoded_answer = base64::encode(serialized_answer);
        return {encoded_answer, ""};
    }

    std::string HandleRequest(ConnectionSocket &connection)
    {
        std::string err;
        auto [is_eof, line, err_readline] = connection.ReadLine();
        if (!err_readline.empty())
        {
            connection.Close();
            return err_readline;
        }
        if (is_eof || (line.size() == 1 && line.back() == '\n' && keepalive_))
        {
            err = connection.Close();
            return err;
        }
        line.pop_back();
        std::string encoded_answer;
        std::tie(encoded_answer, err) = ComputeAnswer(line);
        if (!err.empty())
        {
            return err;
        }
        encoded_answer.push_back('\n');
        err = connection.SendAll(encoded_answer);
        if (!err.empty())
        {
            connection.Close();
            return err;
        }
        return "";
    }

    std::string HandleClient(ConnectionSocket connection)
    {
        std::string err;
        if (keepalive_)
        {
            while (running_ && connection.IsOpen())
            {
                err = HandleRequest(connection);
                if (!err.empty() && !connection.IsOpen())
                {
                    break;
                }
            }
        }
        else
        {
            err = HandleRequest(connection);
        }
        if (auto close_err = connection.Close();
            !close_err.empty() && err.empty())
        {
            err = std::move(close_err);
        }
        return err;
    }

  private:
    TextEmbedder &emb_;
    bool keepalive_;
    const std::atomic_bool &running_;
};
} // namespace neurodaemon
