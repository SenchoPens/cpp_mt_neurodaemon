#pragma once

#include <iostream>
#include <memory>
#include <signal.h>
#include <cstring>
#include <pthread.h>
#include <thread>
#include <vector>

namespace neurodaemon
{
enum class PinMode
{
    no_pin,
    prefer_same_core,
    prefer_other_core,
};

class ThreadManager
{
  public:
    explicit ThreadManager(PinMode pin) : n_cpus_(std::thread::hardware_concurrency()), pin_(pin) {}

    template <typename... Args> void spawnThread(Args &&...args)
    {
        toggle_sigint(SIG_BLOCK);
        threads_.emplace_back(std::forward<Args>(args)...);
        toggle_sigint(SIG_UNBLOCK);
        if (pin_ != PinMode::no_pin) {
            cpu_set_t cpuset;
            CPU_ZERO(&cpuset);
            if (pin_ == PinMode::prefer_same_core) {
                CPU_SET((rr_counter_ % 2) * (n_cpus_ / 2) + (rr_counter_ / 2) % (n_cpus_ / 2), &cpuset);
                ++rr_counter_;
            }
            else {
                CPU_SET((rr_counter_++) % n_cpus_, &cpuset);
            }
            int rc = pthread_setaffinity_np(threads_.back().native_handle(),
                                            sizeof(cpu_set_t), &cpuset);
            if (rc != 0) {
                std::cout << std::strerror(errno) << std::endl;
            }
        }
    }

    void Join()
    {
        for (auto &thread : threads_)
        {
            if (thread.joinable())
            {
                thread.join();
            }
        }
    }

    ~ThreadManager() { Join(); }

  private:
    bool toggle_sigint(int how)
    {
        sigset_t signal_mask;
        sigemptyset(&signal_mask);
        sigaddset(&signal_mask, SIGINT);
        return pthread_sigmask(how, &signal_mask, NULL) == 0;
    }

    std::vector<std::thread> threads_;
    uint32_t n_cpus_;
    PinMode pin_;
    uint32_t rr_counter_ = 0;
};
} // namespace neurodaemon
