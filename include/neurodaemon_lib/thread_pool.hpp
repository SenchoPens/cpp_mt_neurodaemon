#pragma once

#include <neurodaemon_lib/connection_handler.hpp>

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <deque>
#include <future>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <string>

using namespace std::chrono_literals;

namespace neurodaemon
{
template <typename Handler, typename ThreadManager> class ThreadPool
{
  public:
    ThreadPool(uint32_t low_watermark, uint32_t high_watermark,
               std::chrono::seconds above_watermark_timeout, uint32_t max_tasks,
               uint32_t tasks_watermark_threshold,
               const std::atomic_bool &running, Handler &handler,
               ThreadManager &thread_mgr)
        : running_(running), low_watermark_(low_watermark),
          high_watermark_(high_watermark),
          tasks_watermark_threshold_(tasks_watermark_threshold),
          max_tasks_(max_tasks),
          above_watermark_timeout_(above_watermark_timeout), handler_(handler),
          thread_mgr_(thread_mgr)
    {
    }

    void InitThreads()
    {
        for (uint32_t i = 0; i < low_watermark_; ++i)
        {
            thread_mgr_.spawnThread(
                &ThreadPool<Handler, ThreadManager>::RunThreadPoolWorker,
                std::ref(*this), n_active_tasks_++);
        }
    }

    void RunThreadPoolWorker(uint32_t worker_id)
    {
        while (running_)
        {
            auto maybeTask = WaitForTask();
            if (maybeTask)
            {
                std::string err =
                    handler_.HandleClient(std::move(maybeTask->connection));
                maybeTask->promise.set_value(std::move(err));
            }
            else if (worker_id > low_watermark_)
            {
                --n_active_tasks_;
                return;
            }
        }
    }

    void CheckFutures(bool wait = false)
    {
        // std::cout << "Checking future statuses" << std::endl;
        std::lock_guard<std::mutex> lock(mx_);
        for (size_t i = 0; i < results_.size(); ++i)
        {
            auto result = std::move(results_.front());
            results_.pop();
            if (!wait &&
                result.future.wait_for(0s) != std::future_status::ready)
            {
                results_.push(std::move(result));
                continue;
            }
            if (wait)
            {
                result.future.wait();
            }
            std::string err;
            try
            {
                err = result.future.get();
            }
            catch (const std::exception &e)
            {
                std::cout << "Future of task " << result.connection << "thrown "
                          << e.what() << std::endl;
                continue;
            }
            if (!err.empty())
            {
                std::cout << "Task " << result.connection
                          << " completed with non-empty error status " << err
                          << std::endl;
            }
        }
        // std::cout << "Finished checking future statuses" << std::endl;
    }

    std::string HandleClient(ConnectionSocket connection)
    {
        // CheckFutures();
        uint32_t tasks_size;
        {
            std::unique_lock<std::mutex> tasks_lock{mx_};
            if (tasks_.size() >= max_tasks_)
            {
                return connection.Close();
            }
            std::promise<std::string> promise;
            auto future = promise.get_future();
            tasks_.push(Task{.connection = std::move(connection),
                             .promise = std::move(promise)});
            results_.push(TaskResult{.connection = tasks_.back().connection,
                                     .future = std::move(future)});
            tasks_size = tasks_.size();
            // std::cout << "Pushed client fd " << tasks_.back().connection.GetFd()
            //           << std::endl;
        }
        can_work_.notify_one();
        if (tasks_size >= tasks_watermark_threshold_ &&
            n_active_tasks_ < high_watermark_)
        {
            std::cout << "Raising watermark to " << (n_active_tasks_ + 1)
                      << ". tasks_size=" << tasks_size << std::endl;
            thread_mgr_.spawnThread(
                &ThreadPool<Handler, ThreadManager>::RunThreadPoolWorker,
                std::ref(*this), n_active_tasks_++);
            return "";
        }
        return "";
    }

    void NotifyAll() { can_work_.notify_all(); }

  private:
    struct Task
    {
        ConnectionSocket connection;
        std::promise<std::string> promise;
    };

    struct TaskResult
    {
        const ConnectionSocket &connection;
        std::future<std::string> future;
    };

    std::optional<Task> WaitForTask()
    {
        std::optional<Task> task;
        {
            std::unique_lock<std::mutex> tasks_lock{mx_};
            can_work_.wait_for(tasks_lock, above_watermark_timeout_,
                               [&]() { return !tasks_.empty() || !running_; });
            if (tasks_.empty() || !running_)
            {
                return {};
            }
            task = std::move(tasks_.front());
            tasks_.pop();
        }
        return task;
    }

    std::atomic<uint32_t> n_active_tasks_ = 0;
    std::mutex mx_;
    std::queue<Task, std::deque<Task>> tasks_;
    std::condition_variable can_work_;
    std::queue<TaskResult, std::deque<TaskResult>> results_;
    const std::atomic_bool &running_;
    const uint32_t low_watermark_;
    const uint32_t high_watermark_;
    const uint32_t tasks_watermark_threshold_;
    const uint32_t max_tasks_;
    std::chrono::seconds above_watermark_timeout_;
    Handler &handler_;
    ThreadManager &thread_mgr_;
};
} // namespace neurodaemon
