#pragma once

#include <neurodaemon_lib/connection_handler.hpp>

#include <atomic>
#include <cstring>
#include <mutex>
#include <string>

using namespace std::chrono_literals;

namespace neurodaemon
{
template <typename Handler, typename ThreadManager> class ThreadedHandler
{
  public:
    ThreadedHandler(Handler &handler, ThreadManager &thread_mgr,
                    std::mutex &cerr_mutex)
        : handler_(handler), thread_mgr_(thread_mgr), cerr_mutex_(cerr_mutex)
    {
    }

    void RunThreadPerRequestWorker(ConnectionSocket connection)
    {
        auto err = handler_.HandleClient(std::move(connection));
        if (!err.empty())
        {
            std::lock_guard<std::mutex> cerr_lock{cerr_mutex_};
            std::cerr << std::strerror(errno) << std::endl;
        }
    }

    void HandleClient(ConnectionSocket connection)
    {
        thread_mgr_.spawnThread(
            &ThreadedHandler<Handler, ThreadManager>::RunThreadPerRequestWorker,
            std::ref(*this), std::move(connection));
    }

  private:
    Handler &handler_;
    ThreadManager &thread_mgr_;
    std::mutex &cerr_mutex_;
};
} // namespace neurodaemon
