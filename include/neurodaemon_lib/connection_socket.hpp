#pragma once

#include <iostream>
#include <string>
#include <string_view>

namespace neurodaemon
{

class ConnectionSocket
{
  public:
    explicit ConnectionSocket(int fd, bool open = true) noexcept;

    ConnectionSocket(const ConnectionSocket &) = delete;

    ConnectionSocket &operator=(const ConnectionSocket &) = delete;

    ConnectionSocket(ConnectionSocket &&moved) noexcept;

    ConnectionSocket &operator=(ConnectionSocket &&moved) noexcept;

    ~ConnectionSocket();

    bool IsOpen() const;

    int GetFd() const;

    std::string Close();

    std::tuple<bool, std::string, std::string> ReadLine();

    std::string SendAll(std::string_view bytes);

    friend std::ostream &operator<<(std::ostream &stream,
                                    const ConnectionSocket &connection);

  private:
    int fd_;
    bool open_;
};

} // namespace neurodaemon
