#pragma once

#include <string>
#include <string_view>

// Implementation of Base64 from
// https://datatracker.ietf.org/doc/html/rfc4648.html
namespace base64
{
std::string encode(std::string_view bytes);

std::pair<std::string, std::string> decode(std::string_view encoded);
} // namespace base64
