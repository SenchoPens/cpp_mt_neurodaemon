#include "neurodaemon_lib/connection_socket.hpp"

#include <cstring>
#include <memory>
#include <stdexcept>
#include <string>
#include <string_view>
#include <unistd.h>

namespace neurodaemon
{

ConnectionSocket::ConnectionSocket(int fd, bool open) noexcept
    : fd_(fd), open_(open){};

ConnectionSocket::ConnectionSocket(ConnectionSocket &&moved) noexcept
    : ConnectionSocket(moved.fd_, moved.open_)
{
    moved.open_ = false;
}

ConnectionSocket &ConnectionSocket::operator=(ConnectionSocket &&moved) noexcept
{
    fd_ = moved.fd_;
    open_ = moved.open_;
    moved.open_ = false;
    return *this;
}

ConnectionSocket::~ConnectionSocket()
{
    if (open_)
    {
        close(fd_);
    }
}

bool ConnectionSocket::IsOpen() const { return open_; }

int ConnectionSocket::GetFd() const { return fd_; };

std::string ConnectionSocket::Close()
{
    if (open_ && close(fd_) == -1)
    {
        return std::strerror(errno);
    }
    open_ = false;
    return "";
}

// modified, original source:
// https://man7.org/tlpi/code/online/dist/sockets/read_line.c.html
std::tuple<bool, std::string, std::string> ConnectionSocket::ReadLine()
{
    std::string result;
    while (true)
    {
        char ch = 0;
        // unoptimized - it would be faster, but a bit more complex, to read
        // into a bigger buffer
        ssize_t numRead = read(fd_, &ch, 1);

        if (numRead == -1)
        {
            if (errno == EINTR)
            {
                continue;
            }
            return {false, std::move(result), std::strerror(errno)};
        }
        else if (numRead == 0) // EOF
        {
            return {true, std::move(result), ""};
        }
        else
        {
            result.push_back(ch);
            if (ch == '\n')
            {
                break;
            }
        }
    }
    return {false, std::move(result), ""};
}

std::string ConnectionSocket::SendAll(std::string_view bytes)
{
    std::string_view not_sent = bytes;
    while (!not_sent.empty())
    {
        ssize_t numWritten = write(fd_, not_sent.data(), not_sent.size());

        if (numWritten == -1)
        {
            if (errno == EINTR)
            {
                continue;
            }
            else
            {
                return std::strerror(errno);
            }
        }
        else
        {
            not_sent = not_sent.substr(numWritten);
        }
    }
    return {};
}

std::ostream &operator<<(std::ostream &stream,
                         const ConnectionSocket &connection)
{
    return stream << "ConnectionSocket{fd=" << connection.fd_
                  << ", open=" << connection.open_ << "}";
}

} // namespace neurodaemon
