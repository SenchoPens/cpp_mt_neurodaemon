#include "base64/base64.hpp"

#include <array>
#include <string>
#include <string_view>
#include <utility>

// Implementation of Base64 from
// https://datatracker.ietf.org/doc/html/rfc4648.html
namespace base64
{
namespace
{
const std::string_view SEXTET2CHAR =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
const char PADDING = '=';
const unsigned char INVALID = 66;
// from
// https://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64
const std::array<unsigned char, 256> DECODING_TABLE = {
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 64, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 62, 66, 66, 66, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60,
    61, 66, 66, 66, 0,  66, 66, 66, 0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 66, 66, 66, 66,
    66, 66, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
    43, 44, 45, 46, 47, 48, 49, 50, 51, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66};
} // namespace

std::string encode(std::string_view bytes)
{
    std::string encoded((bytes.size() + 2) / 3 * 4, PADDING);
    size_t i;
    for (i = 0; 3 * i + 3 <= bytes.size(); ++i)
    {
        encoded[4 * i + 0] = SEXTET2CHAR[(bytes[3 * i + 0] & 0b11111100) >> 2];
        encoded[4 * i + 1] =
            SEXTET2CHAR[((bytes[3 * i + 0] & 0b00000011) << 4) +
                        ((bytes[3 * i + 1] & 0b11110000) >> 4)];
        encoded[4 * i + 2] =
            SEXTET2CHAR[((bytes[3 * i + 1] & 0b00001111) << 2) +
                        ((bytes[3 * i + 2] & 0b11000000) >> 6)];
        encoded[4 * i + 3] = SEXTET2CHAR[bytes[3 * i + 2] & 0b00111111];
    }
    if (3 * i + 1 == bytes.size())
    {
        encoded[4 * i + 0] = SEXTET2CHAR[(bytes[3 * i + 0] & 0b11111100) >> 2];
        encoded[4 * i + 1] = SEXTET2CHAR[(bytes[3 * i + 0] & 0b00000011) << 4];
        encoded[4 * i + 2] = PADDING;
        encoded[4 * i + 3] = PADDING;
    }
    else if (3 * i + 2 == bytes.size())
    {
        encoded[4 * i + 0] = SEXTET2CHAR[(bytes[3 * i + 0] & 0b11111100) >> 2];
        encoded[4 * i + 1] =
            SEXTET2CHAR[((bytes[3 * i + 0] & 0b00000011) << 4) +
                        ((bytes[3 * i + 1] & 0b11110000) >> 4)];
        encoded[4 * i + 2] = SEXTET2CHAR[(bytes[3 * i + 1] & 0b00001111) << 2];
        encoded[4 * i + 3] = PADDING;
    }
    return encoded;
}

std::pair<std::string, std::string> decode(std::string_view encoded)
{
    if (encoded.size() == 0)
    {
        return {"", ""};
    }
    if (encoded.size() % 4 != 0)
    {
        return {"",
                "length of base64-encoded string must be divisible by four"};
    }
    bool ok = true;
    for (size_t i = 0; i < encoded.size(); ++i)
    {
        ok = ok && DECODING_TABLE[encoded[i]] != INVALID &&
             !(i < encoded.size() - 2 && encoded[i] == PADDING);
    }
    ok = ok && !(encoded[encoded.size() - 2] == PADDING &&
                 encoded[encoded.size() - 1] != PADDING);
    if (!ok)
    {
        return {"", "invalid character in base64-encoded string"};
    }
    std::string decoded(encoded.size() / 4 * 3, '!');
    for (size_t i = 0; 4 * i + 4 <= encoded.size(); ++i)
    {
        decoded[3 * i + 0] = (DECODING_TABLE[encoded[4 * i + 0]] << 2) +
                             (DECODING_TABLE[encoded[4 * i + 1]] >> 4);
        decoded[3 * i + 1] = (DECODING_TABLE[encoded[4 * i + 1]] << 4) +
                             (DECODING_TABLE[encoded[4 * i + 2]] >> 2);
        decoded[3 * i + 2] = (DECODING_TABLE[encoded[4 * i + 2]] << 6) +
                             DECODING_TABLE[encoded[4 * i + 3]];
    }
    if (encoded[encoded.size() - 2] == PADDING)
    {
        decoded.pop_back();
    }
    if (encoded[encoded.size() - 1] == PADDING)
    {
        decoded.pop_back();
    }
    return {decoded, ""};
}
} // namespace base64
