# 1

```
$ ./scripts/simple_load data/1000-most-comon-words_encoded.txt 5
rps: 19.79768952036911
max request processing time: 0.08888556000601966
min request processing time: 0.04473979400063399
avg request processing time: 0.05051094467216171
err_cnt: 0
```

# 2

![pic](img/rps-avg.png)

Замеры и графики были получены с помощью автоматизированных скриптов из `projects/experiment`. Замеры можно попытаться воспроизвести с помощью `gsl.sh`, а графики - с помощью `gsl-plot.sh`. Все замеры были проведены с `-O2`, хотя, с первого взгляда, и `-O0` дает похожие результаты. Я замерял работу сервера в шести вариантах, в зависимиости от режима многопоточности (thread-pool или thread-per-request), от наличия keepalive, а также, в случае thread-per-request, от включения ручного round-robin CPU affinity пиннинга (см. thread_manager.hpp). Я отключил всевозможные менеджеры питания (upower и т.д.) и установил CPU governor на powersave. Между запусками проводился cooldown в 25 секунд.

Начнем со второй группы параметров (--parallel от 8 до 512), на графике это второй столбец. Во всех случаях `RPS` почти не меняется, а Avg растет линейно (видимо, реализуется худший для этой метрики сценарий скедулинга а-ля обход в ширину) --- все ок. Графики RPS почти полностью совпадают с instructions per cycle, разве что линии keepalive  на графике RPS чуть повыше, чем на графке IPC, наверное, из-за меньшей нагрузки на ОС. Самый лучший RPS на 512 потоках дают самые простые варианты - thread-per-request без keepalive, хотя по остальным perf метрикам они где-то по середине.

![pic](img/ipc.png)

Более интересна первая группа параметров. Самые лучшие результаты дают варианты в режиме keepalive --- 60+ rps на 4 потоках. На моем процессоре 4 ядра с 2 гипертредами. Сначала посмотрим на RPS. Замедление на parallel > 4 происходит из-за contention на уровне одного ядра и на уровне процессора --- instructions per cycle понижается до крайне низких значений в районе 0.5, значит, процессор часто чего-то ждет.

Моя гипотеза: так как подавляющая часть времени обработки запроса заключается в перемножении матриц нейросети, то есть два потенциальных боттлнека:
1. арифметические операции,
2. загрузка весов из памяти.

Я не знаю, как проверить ALU contention. Однако второй боттлнек явно реализуется.

![pic](img/cache.png)

Как мы видим, начиная с 5 потоков растут cache load misses и по L1 кэшу (по одному на ядро), и по L3 кэшу (общий на все ядра). Можно заметить, что количество cache load misses по L3 кэшу обратно пропорционально instructions per cycle, что косвенно подтверждает то, что второй боттлнек имеет наибольшее влияние на производительность.

Для полноты картины, остальные метрики perf:

![pic](img/misc.png)

Все графики в одной картинке: `img/gsl.png`

### Параметры моей системы
```
Architecture:            x86_64
  CPU op-mode(s):        32-bit, 64-bit
  Address sizes:         39 bits physical, 48 bits virtual
  Byte Order:            Little Endian
CPU(s):                  8
  On-line CPU(s) list:   0-7
Vendor ID:               GenuineIntel
  Model name:            Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz
    CPU family:          6
    Model:               142
    Thread(s) per core:  2
    Core(s) per socket:  4
    Socket(s):           1
    Stepping:            10
    CPU(s) scaling MHz:  35%
    CPU max MHz:         3400.0000
    CPU min MHz:         400.0000
    BogoMIPS:            3600.00
    Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp l
                         m constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3
                          sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault
                         epb ssbd ibrs ibpb stibp tpr_shadow flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushop
                         t intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp vnmi md_clear flush_l1d arch_capabilities
Virtualization features:
  Virtualization:        VT-x
Caches (sum of all):
  L1d:                   128 KiB (4 instances)
  L1i:                   128 KiB (4 instances)
  L2:                    1 MiB (4 instances)
  L3:                    6 MiB (1 instance)
NUMA:
  NUMA node(s):          1
  NUMA node0 CPU(s):     0-7
Vulnerabilities:
  Gather data sampling:  Mitigation; Microcode
  Itlb multihit:         KVM: Mitigation: VMX disabled
  L1tf:                  Mitigation; PTE Inversion; VMX conditional cache flushes, SMT vulnerable
  Mds:                   Mitigation; Clear CPU buffers; SMT vulnerable
  Meltdown:              Vulnerable
  Mmio stale data:       Mitigation; Clear CPU buffers; SMT vulnerable
  Retbleed:              Vulnerable
  Spec rstack overflow:  Not affected
  Spec store bypass:     Mitigation; Speculative Store Bypass disabled via prctl
  Spectre v1:            Mitigation; usercopy/swapgs barriers and __user pointer sanitization
  Spectre v2:            Vulnerable, IBPB: disabled, STIBP: disabled, PBRSB-eIBRS: Not affected
  Srbds:                 Mitigation; Microcode
  Tsx async abort:       Not affected
```
