#pragma once

#include "neurodaemon_lib/embedding_server.hpp"
#include "neurodaemon_lib/thread_manager.hpp"

#include <string_view>
#include <utility>

namespace neurodaemon
{
std::optional<ThreadingMode> readThreadingMode(std::string_view str_mode)
{
    if (str_mode == "single-thread")
    {
        return ThreadingMode::single_thread;
    }
    if (str_mode == "thread-per-request")
    {
        return ThreadingMode::thread_per_request;
    }
    if (str_mode == "thread-pool")
    {
        return ThreadingMode::thread_pool;
    }
    return {};
}

std::optional<PinMode> readPinMode(std::string_view str_mode)
{
    if (str_mode == "no-pin")
    {
        return PinMode::no_pin;
    }
    if (str_mode == "prefer-same-core")
    {
        return PinMode::prefer_same_core;
    }
    if (str_mode == "prefer-other-core")
    {
        return PinMode::prefer_other_core;
    }
    return {};
}

std::optional<bool> readBoolFlag(std::string_view str_flag)
{
    if (str_flag == "true")
    {
        return true;
    }
    if (str_flag == "false")
    {
        return false;
    }
    return {};
}
} // namespace neurodaemon
