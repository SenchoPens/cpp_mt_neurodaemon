#include "base64/base64.hpp"
#include "neuro.pb.h"
#include "neurodaemon_lib/embedding_server.hpp"
#include "neurodaemon_lib/thread_manager.hpp"
#include "text_process/neuro_predict.hpp"
#include "util.cpp"

#include <iostream>
#include <memory>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdexcept>
#include <string.h>
#include <string>
#include <string_view>
#include <sys/socket.h>
#include <utility>

std::string_view PORT = "50000";
const int BACKLOG = 128;

using Server = neurodaemon::EmbeddingServer<text_process::TextEmbedder,
                                            neurodaemon::ThreadManager>;

std::unique_ptr<Server> server;

void handle_sigint(int signo)
{
    std::cout << "Shuttind down" << std::endl;
    server->Shutdown();
    std::cout << "Closed all sockets" << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        std::cout << "Usage: " << argv[0]
                  << " threading-mode pin-mode keepalive" << std::endl;
        return EXIT_FAILURE;
    }
    auto threading_mode = neurodaemon::readThreadingMode(argv[1]);
    auto pin_mode = neurodaemon::readPinMode(argv[2]);
    auto keepalive = neurodaemon::readBoolFlag(argv[3]);
    if (!threading_mode || !pin_mode || !keepalive)
    {
        std::cout << "Usage: " << argv[0]
                  << " threading-mode pin-mode keepalive" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "Server starting" << std::endl;
    text_process::TextEmbedder emb("data/e5/sentencepiece.bpe.model",
                                   "data/e5/traced_e5.pt");
    std::cout << "Embeddings loaded" << std::endl;

    addrinfo *result;
    addrinfo hints;
    memset(&hints, 0, sizeof(addrinfo));
    hints.ai_canonname = nullptr;
    hints.ai_addr = nullptr;
    hints.ai_next = nullptr;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_family = AF_INET;
    hints.ai_flags = AI_PASSIVE | AI_NUMERICSERV;
    int s = getaddrinfo(nullptr, PORT.data(), &hints, &result) != 0;
    if (s != 0)
    {
        std::cerr << "getaddrinfo: " << gai_strerror(s) << std::endl;
        return EXIT_FAILURE;
    }
    int sfd =
        socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (sfd == -1)
    {
        std::cerr << "socket: " << std::strerror(errno) << std::endl;
        return EXIT_FAILURE;
    }
    int optval = 1;
    if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) ==
        -1)
    {
        std::cerr << "setsockopt: " << std::strerror(errno) << std::endl;
        return EXIT_FAILURE;
    }
    if (bind(sfd, result->ai_addr, result->ai_addrlen) == -1)
    {
        std::cerr << "bind: " << std::strerror(errno) << std::endl;
        return EXIT_FAILURE;
    }
    freeaddrinfo(result);
    if (listen(sfd, BACKLOG) == -1)
    {
        std::cerr << "listen: " << std::strerror(errno) << std::endl;
        return EXIT_FAILURE;
    }

    server = std::make_unique<Server>(emb, sfd, *threading_mode, *pin_mode,
                                      *keepalive);
    signal(SIGPIPE, SIG_IGN);
    struct sigaction sa;
    sa.sa_handler = handle_sigint;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGINT, &sa, nullptr) == -1)
    {
        std::cerr << "sigaction: " << std::strerror(errno) << std::endl;
        return EXIT_FAILURE;
    }
    server->RunMainThread();
    return EXIT_SUCCESS;
}
