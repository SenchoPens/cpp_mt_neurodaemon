#include "neuro.pb.h"
#include "base64/base64.hpp"

#include <fstream>
#include <iostream>
#include <string>

int main(int argc, char* argv[]) {
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " filename" << std::endl;
        return EXIT_FAILURE;
    }
    std::ofstream out{argv[1]};
    std::string word;
    std::string serialized;
    for (uint32_t qid = 0; std::cin >> word; ++qid)
    {
        text_process::NeuroRequest request;
        request.set_qid(qid);
        request.set_query(word);
        if (!request.SerializeToString(&serialized)) {
            return EXIT_FAILURE;
        }
        out << base64::encode(serialized) << std::endl;
    }
}
