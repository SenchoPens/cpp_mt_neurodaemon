#!/usr/bin/env sh

[ $USER = "root" ] || { echo 'Run as root!' && exit 1; }
# jq -v || { echo 'Run as root!' && exit 1; }

export PORT=50000
export SLEEP_T=8
export COOLDOWN_T=30
export DAEMON_PATH=$1
export DAEMON_ARGS=$2
export INPUT_FILE=$3
export PARALLEL_LIST=$4
export EXPERIMENT_NAME=$5
export TMP_DIR="/tmp/nd-experiment/$(date +'%Y_%m_%d_%I_%M_%p')"
export LOAD_SCRIPT=""
export PROJECT_ROOT=$(git rev-parse --show-toplevel)
export EXPERIMENT_DIR=$PROJECT_ROOT/projects/experiment
export OUT_DIR=$PROJECT_ROOT/experiments/$EXPERIMENT_NAME

mkdir -p "$TMP_DIR/load"
mkdir -p "$TMP_DIR/perf"
mkdir -p $OUT_DIR

$DAEMON_PATH $DAEMON_ARGS &
DAEMON_PID=$!
sleep $SLEEP_T  # sleep until the daemon has loaded the embeddings

echo "STARTING TO LOAD"

for PARALLEL in $(echo $PARALLEL_LIST | python3 -c 'print(*eval(input()), sep="\n")'); do
    echo "PROCESSING $PARALLEL"
    export PARALLEL
    perf stat -j -o "$TMP_DIR/perf/$PARALLEL.json" -d -d -d \
        -p $DAEMON_PID \
        $EXPERIMENT_DIR/load.sh "$TMP_DIR/load/$PARALLEL.json"
    echo "COOLING DOWN"
    sleep $COOLDOWN_T
done

echo "FINISHED"

kill -2 $DAEMON_PID

python3 $EXPERIMENT_DIR/experiment_concat.py
