#!/usr/bin/env python3

# I generated most of it with chatgpt

import json
import re
import sys

def parse_input(input_str):
    # Define regex patterns to extract relevant information
    patterns = {
        'queries': r'queries\s*=\s*(\d+)',
        'errors': r'errors\s*=\s*(\d+)',
        'timeouts': r'timeouts\s*=\s*(\d+)',
        'total': r'Total\s*=\s*(\d+)',
        'rps': r'rps\s*=\s*([\d.]+)',
        'total_time': r'total time\s*=\s*([\d.]+)',
        'avg': r'Avg\s*=\s*([\d.]+)',
        'min': r'min\s*=\s*([\d.]+)',
        'max': r'max\s*=\s*([\d.]+)',
        'q99': r'99%\s*=\s*([\d.]+)'
    }
    
    result = {}
    for key, pattern in patterns.items():
        match = re.search(pattern, input_str)
        if match:
            result[key] = float(match.group(1))
    
    return result

def generate_json(input_str):
    data = parse_input(input_str)
    total_time = data['total_time']
    avg = data['avg']
    min_val = data['min']
    max_val = data['max']
    q99 = data['q99']

    total_queries = data.get('queries', 0)
    total_errors = data.get('errors', 0)
    total_timeouts = data.get('timeouts', 0)
    total = data.get('total', 0)
    rps = data.get('rps', 0)

    json_data = {
        "ok_queries": total_queries,
        "errors": total_errors,
        "timeouts": total_timeouts,
        "total": total,
        "rps": rps,
        "total time": total_time,
        "avg": avg,
        "min": min_val,
        "max": max_val,
        "q99": q99
    }

    return json.dumps(json_data, indent=4)

if __name__ == "__main__":
    input_str = sys.stdin.read().strip()
    output_json = generate_json(input_str)
    print(output_json)
