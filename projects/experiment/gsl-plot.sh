#!/usr/bin/env sh

./projects/experiment/plot.py experiments/general-straight-low_aggregated.json img/rps-avg.png \
    'load/rps' 'RPS' \
    'load/avg' 'Average Response Time' \

./projects/experiment/plot.py experiments/general-straight-low_aggregated.json img/ipc.png \
    'perf/instructions/metric-value' 'Instructions Per Cycle' \

./projects/experiment/plot.py experiments/general-straight-low_aggregated.json img/cache.png \
    'perf/LLC-load-misses/metric-value' 'L3 Cache Load Misses Per Second' \
    'perf/L1-dcache-load-misses/metric-value' 'L1 Dcache Load Misses Per Second' \

./projects/experiment/plot.py experiments/general-straight-low_aggregated.json img/misc.png \
    'perf/task-clock/metric-value' 'CPUs Utilized' \
    'perf/cycles/metric-value' 'GHz' \
    'perf/page-faults/metric-value' 'Page Faults Per Second' \
    'perf/dTLB-load-misses/metric-value' 'dTLB Load Misses Per Second' \
    'perf/cpu-migrations/metric-value' 'CPU Migrations Per Second' \
    'perf/context-switches/metric-value' 'Context Switches Per Second' \

./projects/experiment/plot.py experiments/general-straight-low_aggregated.json img/gsl.png \
    'load/rps' 'RPS' \
    'load/avg' 'Average Response Time' \
    'perf/instructions/metric-value' 'Instructions Per Cycle' \
    'perf/page-faults/metric-value' 'Page Faults Per Second' \
    'perf/L1-dcache-load-misses/metric-value' 'L1 Dcache Load Misses Per Second' \
    'perf/LLC-load-misses/metric-value' 'L3 Cache Load Misses Per Second' \
    'perf/dTLB-load-misses/metric-value' 'dTLB Load Misses Per Second' \
    'perf/cpu-migrations/metric-value' 'CPU Migrations Per Second' \
    'perf/context-switches/metric-value' 'Context Switches Per Second' \
    'perf/task-clock/metric-value' 'CPUs Utilized' \
    'perf/cycles/metric-value' 'GHz' \
