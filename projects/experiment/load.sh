#!/usr/bin/env sh

KEEPALIVE=$(python3 -c "import sys; print('--keepalive' if sys.argv[-1]=='true' else '')" $DAEMON_ARGS)
echo RUNNING WITH $PROJECT_ROOT/projects/tcp_load/obf_tcp_load_v1.2.py \
    --host localhost \
    --port $PORT \
    --loglevel CRITICAL \
    $KEEPALIVE --parallel $PARALLEL \
    $INPUT_FILE
python3 $PROJECT_ROOT/projects/tcp_load/obf_tcp_load_v1.2.py \
    --host localhost \
    --port $PORT \
    --loglevel CRITICAL \
    $KEEPALIVE --parallel $PARALLEL \
    $INPUT_FILE \
    | python3 $EXPERIMENT_DIR/tcp_load2json.py \
    > $1
