#!/usr/bin/env python3

# I generated most of it with chatgpt

import os
import json

OUT_DIR = os.environ['TMP_DIR']

def parse_json_file(file_path):
    with open(file_path, 'r') as f:
        return json.load(f)

def process_perf_file(file_path):
    perf_data = {}
    with open(file_path, 'r') as f:
        for line in f:
            obj = json.loads(line)
            event = obj.pop("event")
            perf_data[event] = obj
    return perf_data

def main():
    args = os.environ["DAEMON_ARGS"].split()
    threading_mode, pin_mode, keepalive = args
    result = {
        "meta": {
            "threading_mode": threading_mode,
            "pin_mode": pin_mode,
            "keepalive": keepalive == "true",
        },
        "results": {},
    }
    load_dir = os.path.join(OUT_DIR, "load")
    perf_dir = os.path.join(OUT_DIR, "perf")

    load_files = os.listdir(load_dir)
    perf_files = os.listdir(perf_dir)

    for load_file_name in load_files:
        load_num = os.path.splitext(load_file_name)[0]
        load_file_path = os.path.join(load_dir, load_file_name)
        load_data = parse_json_file(load_file_path)

        perf_file_name = load_num + ".json"
        perf_file_path = os.path.join(perf_dir, perf_file_name)
        if perf_file_name in perf_files:
            perf_data = process_perf_file(perf_file_path)
            result['results'][int(load_num)] = {"load": load_data, "perf": perf_data}

    fname = '+'.join(args) + '.json'
    with open(os.path.join(os.environ['OUT_DIR'], fname), 'w') as f:
        print(json.dumps(result, indent=4), file=f)

if __name__ == "__main__":
    main()

