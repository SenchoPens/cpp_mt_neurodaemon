#!/usr/bin/env sh

export DAEMON_PATH=/home/sencho/p/study/hse/course4/cpp/nd-cmake/neurodaemon
export INPUT_FILE=data/1000-most-comon-words_encoded.txt 
export PARALLEL_LIST='[1,2,3,4,5,6,7,8,16,32,64,128,256,512]' 
export EXPERIMENT_NAME=general-straight-low

./projects/experiment/run-config.sh $DAEMON_PATH "thread-per-request no-pin false" $INPUT_FILE $PARALLEL_LIST $EXPERIMENT_NAME
sleep 60
./projects/experiment/run-config.sh $DAEMON_PATH "thread-per-request no-pin true" $INPUT_FILE $PARALLEL_LIST $EXPERIMENT_NAME
sleep 60
./projects/experiment/run-config.sh $DAEMON_PATH "thread-per-request prefer-other-core false" $INPUT_FILE $PARALLEL_LIST $EXPERIMENT_NAME
sleep 60
./projects/experiment/run-config.sh $DAEMON_PATH "thread-per-request prefer-other-core true" $INPUT_FILE $PARALLEL_LIST $EXPERIMENT_NAME
sleep 60
./projects/experiment/run-config.sh $DAEMON_PATH "thread-pool no-pin false" $INPUT_FILE $PARALLEL_LIST $EXPERIMENT_NAME
sleep 60
./projects/experiment/run-config.sh $DAEMON_PATH "thread-pool no-pin true" $INPUT_FILE $PARALLEL_LIST $EXPERIMENT_NAME
