#!/usr/bin/env python3

import json
import sys
from pathlib import Path

from aggregate_result import aggregate_experiment_result

if __name__ == '__main__':
    experiment_dir = Path(sys.argv[1])
    # res_dir = experiment_dir / 'aggregated'
    # res_dir.mkdir(exist_ok=True)
    # for file in experiment_dir.iterdir():
    #     if file.is_file():
    #         res_file = res_dir / file.name
    #         output_json = aggregate_experiment_result(file)
    #         with open(res_file, "w") as outfile:
    #             json.dump(output_json, outfile, indent=4)
    res_file = experiment_dir.parent / (experiment_dir.name + '_aggregated.json')
    res_json = []
    for file in experiment_dir.iterdir():
        if file.is_file():
            output_json = aggregate_experiment_result(file)
            for (metric, res) in output_json['aggregated_experiment']['perf'].items():
                res['unit'] = res['unit'][0]
                res['metric-unit'] = res['metric-unit'][0]
                for val in ('counter-value', 'metric-value'):
                    if res[val][0] != '<not supported>':
                        res[val] = list(map(float, res[val]))
            res_json.append(output_json)
    with open(res_file, "w") as outfile:
        json.dump(res_json, outfile, indent=4)
