#!/usr/bin/env python3

# I generated most of it with chatgpt

import json
import sys

def aggregate_experiment_result(json_file):
    # Function to recursively traverse and aggregate experiment results
    def aggregate_results(result, aggregated_result):
        for key, value in result.items():
            if isinstance(value, dict):
                # If the value is a dictionary, recursively aggregate its contents
                aggregate_results(value, aggregated_result.setdefault(key, {}))
            else:
                # If the value is not a dictionary, append it to the aggregated list
                aggregated_result.setdefault(key, []).append(value)

    # Load JSON file
    with open(json_file, 'r') as f:
        data = json.load(f)
    
    # Initialize aggregated experiment object
    aggregated_experiment = {}
    
    # Initialize experiment names list
    experiment_names = []
    
    # Iterate over experiments
    for experiment_name, experiment_result in sorted(((int(k), v) for (k, v) in data['results'].items())):
        # Add experiment name to the list
        experiment_names.append(experiment_name)
        
        # Recursively aggregate experiment result
        aggregate_results(experiment_result, aggregated_experiment)
    
    # Write the aggregated experiment and experiment names to the final JSON
    final_json = {
        "meta": data['meta'],
        "experiment_names": experiment_names,
        "aggregated_experiment": aggregated_experiment
    }
    
    # Return the final JSON object
    return final_json

# Example usage
if __name__ == '__main__':
    json_file = sys.argv[1]
    output_json = aggregate_experiment_result(json_file)

    # Write the output to a new JSON file
    with open("aggregated_experiments.json", "w") as outfile:
        json.dump(output_json, outfile, indent=4)

    print("Aggregated experiments saved to 'aggregated_experiments.json'")
