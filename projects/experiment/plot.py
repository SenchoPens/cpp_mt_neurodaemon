#!/usr/bin/env python3

import json
import os
import sys
import seaborn as sns
import matplotlib.ticker as mticker
import matplotlib.pyplot as plt


def label(threading_mode, pin_mode, keepalive):
    tm = "Thread Pool" if threading_mode == "thread-pool" else "Thread Per Request"
    kp = ", Keepalive" if keepalive else ""
    pm = ", Pinned" if pin_mode == "prefer-other-core" else ""
    return tm + kp + pm


# def label(threading_mode, pin_mode, keepalive):
#     tm = "Thread Pool" if threading_mode == "thread-pool" else "Thread Per Request"
#     kp = ", Keepalive" if keepalive else ""
#     pm = ", Pinned" if pin_mode == "prefer-other-core" else ""
#     return tm + kp + pm


def generate_plots(json_file, keys_sequences, output_f, seq_what):
    # Load JSON data
    with open(json_file, "r") as f:
        data = json.load(f)

    # Define parameter groups
    group1 = [1, 2, 3, 4, 5, 6, 7, 8]
    group2 = [8, 16, 32, 64, 128, 256, 512]
    groups = [group1, group2]
    params = data[0]["experiment_names"]

    # Plotting
    fig, axes = plt.subplots(
        len(keys_sequences),
        len(groups),
        figsize=(12, 6 * len(keys_sequences)),
        squeeze=False,
    )
    for i, group in enumerate(groups):
        for j, seq in enumerate(keys_sequences):
            ax = axes[j, i]
            for d, s in zip(sorted(
                data, key=lambda d: tuple(d["meta"].values())
            ), ['--', '-', ':', '-.', '--', '-']):
                leaf_data = d["aggregated_experiment"]
                for key in seq:
                    leaf_data = leaf_data[key]
                ys = []
                for idx, param in enumerate(group):
                    ind = params.index(param)
                    ys.append(leaf_data[ind])
                sns.lineplot(
                    x=group,
                    y=ys,
                    ax=ax,
                    marker="o",
                    linestyle=s,
                    # label="+".join(map(str, d["meta"].values())),
                    label=label(**d["meta"]),
                )
            if i == 1:
                # ax.set_xticklabels(map(str, group))
                # ax.get_xaxis().get_major_formatter().labelOnlyBase = False
                # ax.ticklabel_format(style='plain')
                ax.set_xscale("log")
                ax.set(xticks=group)
                ax.set(xticklabels=group)
                # ax.xaxis.set_minor_locator(locmin)
                ax.set_xticks(group)
                if seq == ["load", "avg"]:
                    ax.set_yscale("log")
            ax.set_title(seq_what[j])
            ax.set_xlabel("--parallel")
            ax.set_ylabel("/".join(seq))
            ax.legend()

    plt.savefig(output_f)
    plt.close()


# Example usage
json_file = sys.argv[1]
output_f = sys.argv[2]
keys_sequences = [s.split("/") for s in sys.argv[3::2]]
seq_what = sys.argv[4::2]

generate_plots(json_file, keys_sequences, output_f, seq_what)
