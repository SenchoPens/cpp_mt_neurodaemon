#include <utility>
#include <string>

#include "base64/base64.hpp"
#include "catch2/catch_test_macros.hpp"

using namespace base64;

TEST_CASE("Encoding an empty string succeeds", "[base64-encode]")
{
    CHECK(encode("") == "");
}

TEST_CASE("Decoding an empty string succeeds", "[base64-decode]")
{
    CHECK(decode("") == std::pair{std::string{}, std::string{}});
}

TEST_CASE("Encoding one character succeeds", "[base64-encode]")
{
    CHECK(encode("a") == "YQ==");
}

TEST_CASE("Decoding one character succeeds", "[base64-decode]")
{
    CHECK(decode("YQ==") == std::pair{std::string{"a"}, std::string{}});
}

TEST_CASE("Encoding with no padding succeeds", "[base64-encode]")
{
    CHECK(encode("light wor") == "bGlnaHQgd29y");
}

TEST_CASE("Encoding with 1 pad succeeds", "[base64-encode]")
{
    CHECK(encode("light wo") == "bGlnaHQgd28=");
}

TEST_CASE("Encoding with 2 pads succeeds", "[base64-encode]")
{
    CHECK(encode("light w") == "bGlnaHQgdw==");
}

TEST_CASE("Decoding with no padding succeeds", "[base64-decode]")
{
    CHECK(std::pair{std::string{"light wor"}, std::string{}} ==
          decode("bGlnaHQgd29y"));
}

TEST_CASE("Decoding with 1 pad succeeds", "[base64-decode]")
{
    CHECK(std::pair{std::string{"light wo"}, std::string{}} ==
          decode("bGlnaHQgd28="));
}

TEST_CASE("Decoding with 2 pads succeeds", "[base64-decode]")
{
    CHECK(std::pair{std::string{"light w"}, std::string{}} ==
          decode("bGlnaHQgdw=="));
}

TEST_CASE("Encoding a long string succeeds", "[base64-encode]")
{
    CHECK(encode("Many hands make light work.") ==
          "TWFueSBoYW5kcyBtYWtlIGxpZ2h0IHdvcmsu");
}

TEST_CASE("Decoding a long string succeeds", "[base64-decode]")
{
    CHECK(
        std::pair{std::string{"Many hands make light work."}, std::string{}} ==
        decode("TWFueSBoYW5kcyBtYWtlIGxpZ2h0IHdvcmsu"));
}

TEST_CASE("Decoding string with wrong length fails", "[base64-decode]")
{
    auto result = decode("b");
    CHECK_FALSE(result.second.empty());
    CHECK(result.first.empty());
}

TEST_CASE("Decoding character outside of base64 alphabet fails", "[base64-decode]")
{
    auto result = decode("b!ba");
    CHECK_FALSE(result.second.empty());
    CHECK(result.first.empty());
}

TEST_CASE("Decoding string with pad in the middle fails", "[base64-decode]")
{
    auto result = decode("b=ba");
    CHECK_FALSE(result.second.empty());
    CHECK(result.first.empty());
}

TEST_CASE("Decoding string with wrong pad suffix fails", "[base64-decode]")
{
    auto result = decode("bo=a");
    CHECK_FALSE(result.second.empty());
    CHECK(result.first.empty());
}
