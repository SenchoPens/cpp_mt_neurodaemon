#include <atomic>
#include <string>
#include <utility>

#include "catch2/catch_test_macros.hpp"
#include "neurodaemon_lib/connection_handler.hpp"

using namespace neurodaemon;

struct TextEmbedderMock
{
    bool encode_called = false;

    template <typename... Args> std::vector<float> Encode(Args &&...args)
    {
        encode_called = true;
        return {};
    }
};

TEST_CASE("ComputeAnswer succeeds")
{
    TextEmbedderMock mock;
    const std::atomic_bool running = true;
    ConnectionHandler<TextEmbedderMock> handler{mock, false, running};
    auto [ans, err] = handler.ComputeAnswer("");
    CHECK(err.empty());
}
