#include <array>
#include <string>
#include <unistd.h>
#include <utility>

#include "catch2/catch_test_macros.hpp"
#include "neurodaemon_lib/connection_socket.hpp"

using namespace neurodaemon;

TEST_CASE("Open / close state works")
{
    std::array<int, 2> fds;
    if (pipe(fds.data()) == -1) {
        FAIL("Can't create pipe for test");
    }
    ConnectionSocket connection{fds[0], true};
    CHECK(connection.IsOpen());
    auto err = connection.Close();
    CHECK(err.empty());
    CHECK_FALSE(connection.IsOpen());
}
